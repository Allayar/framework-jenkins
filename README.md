## Framework

### Run Smoke Test

To execute the smoke test suite, use the following Maven command:

```bash
Run smoke test:
mvn -Dbrowser=chrome -Denvironment=dev -Dsurefire.suiteXmlFiles=src/test/resources/testng-smoke.xml clean test

Run all test:
mvn -Dbrowser=chrome -Denvironment=dev -Dsurefire.suiteXmlFiles=src/test/resources/testng-all.xml clean test