package com.epam.training.student_allayar_jandullaev.ta_training_java_framework.pages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class AbstractPage {
    protected WebDriver driver;
    protected WebDriverWait wait;
    protected final Logger logger;
    protected final int WAIT_TEN_SECONDS = 10;

    public AbstractPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, Duration.ofSeconds(WAIT_TEN_SECONDS));
        this.logger = LogManager.getRootLogger();
    }
}
