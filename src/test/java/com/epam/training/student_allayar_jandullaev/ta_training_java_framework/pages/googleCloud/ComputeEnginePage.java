package com.epam.training.student_allayar_jandullaev.ta_training_java_framework.pages.googleCloud;

import com.epam.training.student_allayar_jandullaev.ta_training_java_framework.pages.AbstractPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ComputeEnginePage extends AbstractPage {
    private final Logger logger = LogManager.getRootLogger();

    private static final String REGAXOFCOST = "USD [0-9,\\.]+";
    @FindBy(xpath = "//span[contains(text(), 'Compute Engine')]")
    private WebElement selectComputeEngine;
    @FindBy(xpath = "//input[contains(@aria-label, 'quantity')]")
    private WebElement numberOfInstances;
    @FindBy(xpath = "//md-select-value[contains(., 'Free: Debian, CentOS, CoreOS, Ubuntu or BYOL (Bring Your Own License)')]")
    private WebElement selectOperatingSystemSoftware;
    @FindBy(xpath = "//md-option[@value = 'free']//div[contains(text(), 'Free: Debian, CentOS, CoreOS, Ubuntu or BYOL (Bring Your Own License)')]")
    private WebElement operatingSystemItem;
    @FindBy(xpath = "//md-select-value[contains(., 'Regular')]")
    private WebElement selectProvisioningModel;
    @FindBy(xpath = "//md-option[contains(., 'Regular')]")
    private WebElement provisioningModel;
    @FindBy(xpath = "//md-select-value//span//div[contains(., 'General purpose')]")
    private WebElement selectMachineFamily;
    @FindBy(xpath = "//md-option[contains(., 'General purpose')]")
    private WebElement machineFamily;
    @FindBy(xpath = "//md-select-value//span//div[contains(text(), 'E2')]")
    private WebElement selectSeries;
    String seriesLocator = "//md-option[@value='n1' and contains(., '%s')]";
    @FindBy(xpath = "//md-select-value//span//div[contains(text(), 'n1-standard-1 (vCPUs: 1, RAM: 3.75GB)')]")
    private WebElement machineType;
    @FindBy(xpath = "//md-option[contains(., 'n1-standard-8 (vCPUs: 8, RAM: 30GB)')]")
    private WebElement machineTypeItem;
    @FindBy(xpath = "//md-checkbox[@aria-label='Add GPUs']")
    private WebElement addGPUCheckbox;
    @FindBy(xpath = "//md-select[contains(., 'GPU type')]")
    private WebElement GPUModel;
//    @FindBy(xpath = "//md-option[contains(., 'NVIDIA Tesla V100')]")
//    private WebElement GPUModelItem;
    String GPUModelItem = "//md-option[contains(., '%s')]";
    @FindBy(xpath = "//md-select[@placeholder = 'Number of GPUs']")
    private WebElement selectNumberOfGPUs;
//    @FindBy(xpath = "//div[contains(@class, 'md-active')]//md-option[@value = '1']")
//    private WebElement numberOfGPUs;
    String numberOfGPUsItem = "//div[contains(@class, 'md-active')]//md-option[@value = '1']";
    @FindBy(xpath = "//md-select[@placeholder = 'Local SSD']")
    private WebElement selectLocalSSD;

//    @FindBy(xpath = "//md-option[contains(., '2x375 GB')]")
//    private WebElement localSSD;
    String localSSD = "//md-option[contains(., '%s')]";
    @FindBy(xpath = "//md-select[@placeholder = 'Datacenter location']")
    private WebElement datacenterLocation;
    @FindBy(xpath = "//md-option[@value = 'us-central1']//div[contains(., 'Iowa (us-central1)')]")
    private List<WebElement> datacenterLocationItem;
    @FindBy(xpath = "//md-select[@placeholder = 'Committed usage']")
    private WebElement committedUsage;
    @FindBy(xpath = "//md-option[contains(., '1 Year')]")
    private List<WebElement> committedUsageItem;
    @FindBy(xpath = "//button[@type = 'button' and contains(text(), 'Add to Estimate')]")
    private WebElement addToEstimateButton;
    @FindBy(xpath = "//button[@title = 'Email Estimate']")
    private WebElement selectEmailEstimate;
    @FindBy(xpath = "//label[contains(., 'Email')]/following-sibling::input")
    private WebElement enterEmail;
    @FindBy(xpath = "//button[contains(., 'Send Email')]")
    private WebElement sendEmailButton;
    @FindBy(xpath = "//*[contains(text(), 'Total Estimated Cost:')]")
    private WebElement checkPrice;
    public ComputeEnginePage(WebDriver driver) {
        super(driver);
        driver.manage().window().maximize();
        driver.switchTo().frame(0);
        driver.switchTo().frame(0);
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, WAIT_TEN_SECONDS), this);
    }
    public ComputeEnginePage updateDriver(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, Duration.ofSeconds(WAIT_TEN_SECONDS));
        driver.manage().window().maximize();
        driver.switchTo().defaultContent();
        driver.switchTo().frame(0);
        driver.switchTo().frame(0);
        return this;
    }
    public ComputeEnginePage selectComputeEngine() {
        selectComputeEngine.click();
        logger.info("Selected Compute Engine");
        return this;
    }
    public ComputeEnginePage enterNumberOfInstances(String numberOfInstance) {
        numberOfInstances.click();
        numberOfInstances.sendKeys(numberOfInstance);
        logger.info("Entered Number of Instances");
        return this;
    }
    public ComputeEnginePage selectOperatingSystemSoftware() {
        selectOperatingSystemSoftware.click();
        wait.until(ExpectedConditions.visibilityOf(operatingSystemItem)).click();
        logger.info("Selected Operating System");
        return this;
    }
    public ComputeEnginePage clickProvisioningModel() {
        selectProvisioningModel.click();
        wait.until(ExpectedConditions.visibilityOf(provisioningModel)).click();
        logger.info("Clicked Provision Model");
        return this;
    }
    public ComputeEnginePage selectMachineFamily() {
        selectMachineFamily.click();
        wait.until(ExpectedConditions.visibilityOf(machineFamily)).click();
        logger.info("Selected Operating System");
        return this;
    }
    public ComputeEnginePage selectSeries(String seriesItem) {
        selectSeries.click();
        String series = String.format(seriesLocator, seriesItem);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(series))).click();
        logger.info("Selected Series");
        return this;
    }
    public ComputeEnginePage selectMachineType() {
        machineType.click();
        wait.until(ExpectedConditions.visibilityOf(machineTypeItem)).click();
        logger.info("Selected Machine Type");
        return this;
    }
    public ComputeEnginePage clickGPUCheckbox() {
        wait.until(ExpectedConditions.visibilityOf(addGPUCheckbox)).click();
        logger.info("Clicked GPU Checkbox");
        return this;
    }
    public ComputeEnginePage selectGPUModel(String gpuType) {
        GPUModel.click();
        String gpuTypeLocator = String.format(GPUModelItem, gpuType);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(gpuTypeLocator))).click();
        logger.info("Selected GPU Model");
        return this;
    }
    public ComputeEnginePage enterNumberOfGPUs(String numberOfGpus) {
        selectNumberOfGPUs.click();
        String numberOfGPUsLocator = String.format(numberOfGPUsItem, numberOfGpus);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(numberOfGPUsLocator))).click();
        logger.info("Entered Number Of GPUs");
        return this;
    }
    public ComputeEnginePage selectLocalSSD(String localSSDItem) {
        selectLocalSSD.click();
        String localSSDLocator = String.format(localSSD , localSSDItem);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(localSSDLocator))).click();
        logger.info("Selected Local SSD");
        return this;
    }
    public ComputeEnginePage selectDatacenterLocation() {
        datacenterLocation.click();
        wait.until(ExpectedConditions.visibilityOf(datacenterLocationItem.get(0))).click();
        logger.info("Selected Data Center Location");
        return this;
    }
    public ComputeEnginePage selectCommittedUsage() {
        committedUsage.click();
        wait.until(ExpectedConditions.visibilityOf(committedUsageItem.get(0))).click();
        logger.info("Selected Committed Usage");
        return this;
    }
    public ComputeEnginePage addToEstimateButton() {
        addToEstimateButton.click();
        logger.info("Clicked Estimated Button");
        return this;
    }
    public ComputeEnginePage selectEmailEstimate() {
        selectEmailEstimate.click();
        return this;
    }
    public ComputeEnginePage enterEmail() {
        for (int i = 0; i < 1; i++) {
            enterEmail.sendKeys(Keys.TAB);
        }
        enterEmail.sendKeys(Keys.COMMAND + "v");
        logger.info("Entered Email");
        return this;
    }
    public ComputeEnginePage sendEmailButton() {
        sendEmailButton.click();
        logger.info("Send Email");
        return this;
    }
    public String getTotalEstimatedCostIsVisible() {
        return wait.until(ExpectedConditions.visibilityOf(checkPrice)).getText();
    }
    public String getTotalEstimatedCost() {
        String costText = wait.until(ExpectedConditions.visibilityOf(checkPrice)).getText();
        Pattern pattern = Pattern.compile(REGAXOFCOST);
        Matcher matcher = pattern.matcher(costText);
        if (matcher.find()) {
            return matcher.group(0);
        } else {
            throw new RuntimeException("USD number not found in the cost text");
        }
    }
}