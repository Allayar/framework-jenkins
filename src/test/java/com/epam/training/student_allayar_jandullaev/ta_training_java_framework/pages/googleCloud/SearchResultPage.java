package com.epam.training.student_allayar_jandullaev.ta_training_java_framework.pages.googleCloud;

import com.epam.training.student_allayar_jandullaev.ta_training_java_framework.pages.AbstractPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class SearchResultPage extends AbstractPage {
    private final Logger logger = LogManager.getRootLogger();
    @FindBy(xpath = "//a[text() = 'Google Cloud Pricing Calculator']")
    private WebElement linkElement;

    public SearchResultPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, WAIT_TEN_SECONDS), this);
    }
    public ComputeEnginePage clickLink() {
        linkElement.click();
        logger.info("Clicked Google Calculator Link");
        return new ComputeEnginePage(driver);
    }
}
