package com.epam.training.student_allayar_jandullaev.ta_training_java_framework.pages.yopmail;

import com.epam.training.student_allayar_jandullaev.ta_training_java_framework.pages.AbstractPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class YopMailEmailPage extends AbstractPage {
    @FindBy(id = "refresh")
    private WebElement refreshButton;
    @FindBy(id = "mail")
    private WebElement cost;

    static String REGAXOFCOST = "USD [0-9,\\.]+";

    public YopMailEmailPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public YopMailEmailPage updateDriver(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, Duration.ofSeconds(WAIT_TEN_SECONDS));
        return this;
    }
    public YopMailEmailPage refreshButton() throws InterruptedException {
        Thread.sleep(5000);
        refreshButton.click();
        logger.info("Page refreshed");
        return this;
    }

    public String getEstimatedCost() throws InterruptedException {
        Thread.sleep(2000);
        driver.switchTo().frame("ifmail");
        String costText = wait.until(ExpectedConditions.visibilityOf(cost)).getText();
        Pattern pattern = Pattern.compile(REGAXOFCOST);
        Matcher matcher = pattern.matcher(costText);
        if (matcher.find()) {
            return matcher.group(0);
        } else {
            throw new RuntimeException("USD number not found in the cost text");
        }
    }
}
