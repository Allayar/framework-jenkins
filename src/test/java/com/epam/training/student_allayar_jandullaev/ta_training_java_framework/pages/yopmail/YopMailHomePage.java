package com.epam.training.student_allayar_jandullaev.ta_training_java_framework.pages.yopmail;

import com.epam.training.student_allayar_jandullaev.ta_training_java_framework.pages.AbstractPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class YopMailHomePage extends AbstractPage {
    private static final String PAGE_URL = "https://yopmail.com/";
    @FindBy(xpath = "//a[@href = 'email-generator']")
    private WebElement randomEmailGenerator;
    public YopMailHomePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, WAIT_TEN_SECONDS), this);
    }

    public YopMailHomePage openPage() {
        driver.get(PAGE_URL);
        logger.info("Page Opened YopMail");
        return this;
    }

    public YopMailGeneratorPage randomEmailGenerator() {
        wait.until(ExpectedConditions.visibilityOf(randomEmailGenerator)).click();
        driver.get(PAGE_URL + "email-generator");
        logger.info("Clicked Random Email Generator button");
        return new YopMailGeneratorPage(driver);
    }
}
