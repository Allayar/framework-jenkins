package com.epam.training.student_allayar_jandullaev.ta_training_java_framework.test;

import com.epam.training.student_allayar_jandullaev.ta_training_java_framework.pages.yopmail.YopMailHomePage;
import com.epam.training.student_allayar_jandullaev.ta_training_java_framework.util.TabUtils;
import org.testng.Assert;
import org.testng.annotations.Test;
public class TotalEstimatedCostTest extends CommonConditions {
    @Test
    public void testTotalEstimatedMonthlyCost() throws InterruptedException {
        TabUtils.newTab(driver);
        yopMailEmailPage = new YopMailHomePage(driver).openPage()
                .randomEmailGenerator()
                .copyToClipboard()
                .clickCheckInbox();
        TabUtils.switchToGooglePage(driver);
        computeEnginePage = computeEnginePage.updateDriver(driver).enterEmail().sendEmailButton();
        String costInGooglePage = computeEnginePage.getTotalEstimatedCost();
        TabUtils.switchToYopMailPage(driver);
        yopMailEmailPage = yopMailEmailPage.refreshButton();
        String costInYopMailPage = yopMailEmailPage.updateDriver(driver).getEstimatedCost();

        Assert.assertEquals(costInYopMailPage,
                costInGooglePage, "Error: The estimated cost from the email page does not match");

    }
}
